# Add ESLint
See [angular-eslint](https://github.com/angular-eslint/angular-eslint) for current information.

Note that `eslint-plugin-jsdoc` has been added to the install process.

```bash
ng add @angular-eslint/schematics
# Or alpha version for early Angular v13 support (as of 11/5/2021).
ng add @angular-eslint/schematics@next
npm install --save-dev eslint-plugin-jsdoc
```

New script in **package.json**:
```json
"eslint": "eslint ./src --ext .js,.jsx,.ts,.tsx || (exit 0)",
```

Update *.ts overrides (`plugins` is completely new) in **.eslintrc.json**:
```json
      "plugins": [
        "jsdoc"
      ],
      "extends": [
        "plugin:@angular-eslint/recommended",
        "plugin:@angular-eslint/template/process-inline-templates",
        "plugin:jsdoc/recommended"
      ],
      "rules": {
        "@angular-eslint/directive-selector": [
          "error",
          {
            "type": "attribute",
            "prefix": "app",
            "style": "camelCase"
          }
        ],
        "@angular-eslint/component-selector": [
          "error",
          {
            "type": "element",
            "prefix": "app",
            "style": "kebab-case"
          }
        ],
        "jsdoc/require-jsdoc": [1, {
          "contexts": ["ArrowFunctionExpression", "ClassDeclaration", "ClassExpression", "ClassProperty", "FunctionDeclaration", "MethodDefinition", "TSInterfaceDeclaration", "TSEnumDeclaration", "TSEnumMember"]
        }],
        "jsdoc/require-description": [1, {
          "contexts": ["ArrowFunctionExpression", "ClassDeclaration", "ClassExpression", "ClassProperty", "FunctionDeclaration", "MethodDefinition", "TSInterfaceDeclaration", "TSEnumDeclaration", "TSEnumMember"]
        }],
        "jsdoc/require-param-type": "off",
        "jsdoc/require-returns-type": "off"
      }
```

## Test
```bash
npm run eslint
```
