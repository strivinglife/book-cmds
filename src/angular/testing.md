# Testing
## Run unit tests
By default uses [Karma](https://karma-runner.github.io/)/[Jasmine](https://jasmine.github.io/).

```bash
ng test
```

## Add ESLint
See [angular-eslint on GitHub](https://github.com/angular-eslint/angular-eslint) for current information. As of 9/5/2021:
```bash
ng add @angular-eslint/schematics
```
