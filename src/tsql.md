# T-SQL

Get all tables in a database.
```sql
SELECT TABLE_SCHEMA, TABLE_NAME
FROM <database_name>.INFORMATION_SCHEMA.TABLES 
WHERE TABLE_TYPE = 'BASE TABLE'
ORDER BY TABLE_SCHEMA, TABLE_NAME
```

Get all views in a database.
```sql
SELECT TABLE_SCHEMA, TABLE_NAME
FROM <database_name>.INFORMATION_SCHEMA.TABLES 
WHERE TABLE_TYPE = 'VIEW'
ORDER BY TABLE_SCHEMA, TABLE_NAME
```

Get all stored procedures in a database.
```sql
SELECT SPECIFIC_SCHEMA, SPECIFIC_NAME
FROM <database_name>.INFORMATION_SCHEMA.ROUTINES
WHERE ROUTINE_TYPE = 'PROCEDURE'
ORDER BY SPECIFIC_SCHEMA, SPECIFIC_NAME
```

## Table altering, in T-SQL

### Change table schema
Change the schema on an existing table.
```sql
ALTER SCHEMA projectManagement
TRANSFER dbo.RequestPriority
```

### Change column
Alter a column in an existing table.
```sql
ALTER TABLE [projectManagement].[Task]
ALTER COLUMN Name varchar(250) not null
```

### Add identity and primary key
Add a new identity column to an existing table.
```sql
-- See http://stackoverflow.com/a/3698824/11912
ALTER TABLE JobSupplies add Id INT IDENTITY
ALTER TABLE JobSupplies add constraint PK_JobSupplies primary KEY(Id)
```
