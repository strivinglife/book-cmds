# Go

```bash
# Initialize a new module.
go mod init example.com/hello

# Run the current directory.
go run .

# Update and cleanup go.mod.
go mod tidy

# Format all files in directory and subdirectories.
go fmt ./...

# Check for possible logic errors in directory and subdirectories.
go vet ./...

# Run tests (in files that end with _test.go).
go test
go test -v

# testing.Short() returns true. Useful for t.Skip()ing long/integration tests.
go test -short

# Generate a platform-specific application.
go build

# Build with specific name.
go build -o output-name

# Install the current application to the Go path.
go install
```

## Information
```bash
# Get Go's version.
go version

# Find where the current module would be installed to.
go list -f '{{.Target}}'

# List environment information, including where packages are installed via go get.
go env
```

## Requirements
```bash
# Update go.mod to point a module to a local directory.
go mod edit -replace example.com/greetings=../greetings

# Get an external module and add to go.mod require.
go get golang.org/x/example

# Get dependencies for code in the current directory (already added as an import).
go get .
```

## Workspaces
```bash
# Initialize a workspace with an existing module.
go work init ./hello

# Add a child module directory to the workspace.
go work use ./example
```

## Utilities
```bash
# Generate a TLS/SSL cert. Get GOPATH from `go env`.
go run 'C:\Program Files\Go\src\crypto\tls\generate_cert.go' --rsa-bits=2048 --host=localhost
```

## Testing
```bash
# Get basic test coverage, per file.
go test -cover ./...

# Generate a coverage report by method and function.
go test -coverprofile='profile.out' ./...
# Read the report and output to the command line.
go tool cover -func='profile.out'
# Read the report and output to HTML.
go tool cover -func='profile.out'
# Generate a coverage report with number of times each statement is executed during testing.
# Use -covermode=atomic if running any tests in parallel.
go test -covermode=count -coverprofile='profile.out' ./...
```
