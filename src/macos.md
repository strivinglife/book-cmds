# macOS

## Homebrew
```bash
brew update
# Install example.
brew install git
# List outdated formulae.
brew outdated
brew upgrade
```
