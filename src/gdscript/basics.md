# GDScript Basics
- [GDScript reference](https://docs.godotengine.org/en/stable/tutorials/scripting/gdscript/gdscript_basics.html)
	- [GDScript style guide¶](https://docs.godotengine.org/en/stable/tutorials/scripting/gdscript/gdscript_styleguide.html)

## Project Settings
Keep the display of elements on Window changing: Display > Window > Mode > `canvas_items`.

## Physics
- `AnimatableBody2D`: moved by script or animation
- `CharacterBody2D`: moved by script
	- apply velocity
- `RigidBody2D`: moved by physics simulation; have gravity and can be pushed
	- apply force to move
- `StaticBody2D`: static or only moves by script

## Layers and Masks
- Layer: what layer am I on
- Mask: what layer do I check for

Neither needs to be set. For example, if a thing is only checking for something to collide with it, only `Mask` needs to be set. Something like the ground may only have a `Layer` set, but no `Mask`.
