# Visual Studio

Install particular NuGet package.ps1
```bash
# Installs a particular version of a package.
# See http://stackoverflow.com/q/16126338/11912
Install-Package jQuery -Version 1.10.2
```

## Team Foundation Server
> Last reviewed around November 2013.

Delete workspace for user.bat
```bat
REM Run from VS developer command prompt
tf workspace /delete _workspace_;_domain_\_user_ /server:http://_server_:8080/tfs
PAUSE
```

List all workspaces.bat
```bat
REM Run from VS developer command prompt
tf workspaces /server:http://_server_:8080/tfs /owner:*
PAUSE
```
