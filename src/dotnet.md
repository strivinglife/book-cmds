# .NET

- [dotnet new](https://docs.microsoft.com/en-us/dotnet/core/tools/dotnet-new)
	- `dotnet new list`
	- `dotnet new gitignore` adds a .gitignore to the current directory.
	- `dotnet new mvc --auth Individual -o ProjectName` create a new MVC web app with authentication using SQLite
	- `dotnet new sln`
		- `dotnet sln add ProjectName` (don't include `.\` from PowerShell)

```bash
# View .NET installed versions and information.
dotnet --info
```

```bash
# Restore packages.
dotnet restore

dotnet watch run
```

## Environment variables
```bash
# launchSettings.json can typically set these.
# See https://learn.microsoft.com/en-us/aspnet/core/fundamentals/environments?view=aspnetcore-7.0#development-and-launchsettingsjson
$Env:ASPNETCORE_ENVIRONMENT = "Development"
$Env:NETCORE_ENVIRONMENT = "Development"
```

The `ASPNETCORE_ENVIRONMENT` value overrides `DOTNET_ENVIRONMENT`.

## Entity Framework Core Tools
[CLI reference](https://docs.microsoft.com/en-us/ef/core/cli/dotnet).

```bash
# Install a tool globally.
dotnet tool install --global dotnet-ef

# Update a tool to the latest version.
dotnet tool update --global dotnet-ef

# Install a specific version.
dotnet tool install --global dotnet-ef --version 8.0.10
dotnet tool install --global dotnet-ef --version 8.0.10 --allow-downgrade

# List installed tools.
dotnet tool list -g

# Search for tools matching `searchTerm`.
dotnet tool search searchTerm
dotnet tool search dotnet-ef --detail --take 1
```

## Add SQLite
```bash
dotnet add package Microsoft.EntityFrameworkCore.Sqlite
dotnet add package Microsoft.EntityFrameworkCore.Design
```

### appsettings.json
```json
{
  "ConnectionStrings": {
    "DefaultConnection": "Data source=./Database/name.db"
  }
}
```

### Data/DataContext.cs
```csharp
using API.Models;
using Microsoft.EntityFrameworkCore;

namespace API.Data
{
    public class DataContext : DbContext
    {
        public DbSet<Thing> Things { get; set; }

        public DataContext(DbContextOptions options) : base(options) { }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
        }
    }
}
```

### Program.cs
```csharp
// Add near the top.
builder.Services.AddDbContext<DataContext>(options =>
{
    options.UseSqlite(builder.Configuration.GetConnectionString("DefaultConnection"));
});

// Optional: apply migrations on startup. Add after builder.Build().
using (var scope = app.Services.CreateScope()) {
    var db = scope.ServiceProvider.GetRequiredService<DataContext>();
    db.Database.Migrate();
}
```

### Create Initial Migration
```bash
dotnet ef migrations add InitialCreate -o Data/Migrations
dotnet ef database update
```

```bash
# List the last 5 migrations.
dotnet ef migrations list --no-build | Select-Object -last 5
```

```bash
# Create a migration for a specific context.
dotnet ef migrations add AddXEntity -o Data/Migrations/Application -c ApplicationDbContext
dotnet ef database update -c ApplicationDbContext
```
