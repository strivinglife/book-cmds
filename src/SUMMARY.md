# Summary

- [Introduction](./introduction.md)

# Operating Systems

- [Windows Commands](./windows.md)
- [Linux Commands](./linux.md)
	- [System management](./linux/system-management.md)
	- [File system](./linux/file-system.md)
	- [Users and permissions](./linux/users-permissions.md)
	- [nginx](./nginx.md)
	- [Raspberry Pi Commands](./pi.md)
	- [Directories Overview](./linux/directories-overview.md)
- [macOS Commands](./macos.md)

# Programming Languages

- [Angular](./angular.md)
	- [Basics](./angular/basics.md)
	- [Personal standard]()
		- [ESLint](./angular/personal/eslint.md)
		- [GitLab Pages](./angular/personal/gitlab-pages.md)
		- [Jest](./angular/personal/jest.md)
		- [Stylelint](./angular/personal/stylelint.md)
		- [Updating](./angular/personal/updating.md)
	- [Testing](./angular/testing.md)
		- [Fixes](./angular/testing/fixes.md)
	- [Course notes]()
		- [Angular: Getting Started](./angular/courses/angular-getting-started.md)
		- [Angular Component Communication](./angular/courses/angular-component-communication.md)
		- [Related: Creating Layouts with CSS Grid](./angular/courses/related/creating-layouts-css-grid.md)
- [.NET](./dotnet.md)
	- [Examples](./dotnet/examples.md)
- [GDScript](./gdscript/basics.md)
- [Go](./golang.md)

# SQL

- [T-SQL](./tsql.md)
- [PostgreSQL](./postgresql.md)
- [SQLite](./sqlite.md)

# Tools

- [Node Commands](./node.md)
	- [My Node Globals](./node/globals.md)
- [PowerShell Commands](./powershell.md)
	- [PowerShell Profile](./powershell/profile.md)
- [Docker](./docker.md)
- [Windows Terminal](./windows-terminal.md)
- [Visual Studio Commands](./visual-studio.md)
