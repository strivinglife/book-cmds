# My Node Globals
The following is a list of globals I tend to install with Node. Items are listed in order of relative importance, and then alphabetically.

```
npm install ___ -g
npm install -g ___
```

Again, you can run `npm ls -g --depth 0` to view any packages that have been installed globally.

## Node Utilities
- npm-check-updates

## General Utilities
- http-server

## TypeScript
- typescript
- eslint
- typedoc
- dts-gen

## Frameworks
- @angular/cli
- @ionic/cli
- @vue/cli
- create-react-app
- gulp-cli

## Visual Studio Code Extension Development
- yo
- generator-code
