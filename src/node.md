# Node

Upgrade npm
```
npm install npm@latest -g
```

View versions of a package
```bash
npm view package-name versions
```

## Global Packages

Find all globally installed packages
```
npm ls -g --depth 0
```

Install/update a package globally
```
npm install http-server -g
```

Find outdated global packages
```
npm outdated -g --depth=0
```

## Auditing
Find any production packages with vulnerabilities. Id est, ignore any development packages.
```bash
npm audit --production
```

Determine why a package is required.
```bash
npm explain package-name
npm why package-name
npm ls package-name
```


