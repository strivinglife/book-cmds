# nginx

- [Documentation](https://nginx.org/en/docs/)
- [Getting Started Wiki](https://www.nginx.com/resources/wiki/start/)

## Configuration
```bash
# Test/verify configuration.
sudo nginx -t

# View base configuration.
sudo cat /etc/nginx/nginx.conf

# View default site configuration.
sudo cat /etc/nginx/sites-enabled/default

# List enabled sites.
ls -l /etc/nginx/sites-enabled/

# List available sites.
ls /etc/nginx/sites-available/

# Enable site via a symbolic link.
sudo ln -s /etc/nginx/sites-available/SITE_CONFIG_FILE_NAME /etc/nginx/sites-enabled/

# Remove symbolic link/enabled site.
sudo rm SITE_CONFIG_FILE_NAME
sudo unlink SITE_CONFIG_FILE_NAME
# Remove with confirmation.
sudo rm -i SITE_CONFIG_FILE_NAME
```

## Logging
```bash
# View access logs.
sudo cat /var/log/nginx/access.log

# Get top 20 most used user agents.
sudo cat /var/log/nginx/access.log | awk -F\" '{print $6}' | sort | uniq -c | sort -nr | head -20

# Get top 20 most used client IPs.
sudo cat /var/log/nginx/access.log | awk '{print $1}' | sort | uniq -c | sort -nr | head -20
```

## Management
```bash
# Restart nginx.
sudo systemctl restart nginx
```
