# PowerShell

List all files with a particular extension in the current directory and its children, with results sorted by full file name.

```bash
Get-ChildItem -Path .\ -Filter *.sln -Recurse -File | Select Fullname | Sort-Object Fullname
```

List the 10 largest files in the current directory and subdirectories.
```bash
gci -r | sort Length -desc | select @{n="Length";e={$_.length}}, fullname -f 10
```

Search Files.ps1
```bash
Get-ChildItem -Recurse -Include *.item | select-string "<term>"
gci -r -i *.item | select-string "<term>"
gci -r -i *.* -exclude *.dll,*.xml | select-string "<term>"
```

Search with Git Grep.ps1
```bash
# Can only be run within a Git repository, but this will also search untracked files, as well as those that are tracked.
git grep --untracked '<term>'
```

View a file's contents
```bash
Get-Content .\path\to\file.ext
```

Find path of a binary.
```bash
where.exe git.exe
```

Run `git push` on all child folders of the current directory.
```bash
gci -Directory | % { Push-Location $_.FullName; git push; Pop-Location }
Get-ChildItem -Directory | foreach { Push-Location $_.FullName; git push; Pop-Location }
```

## Environment variables
Get all environment variables.
```bash
dir Env:
```

Get environment variables at a certain scope.
```bash
[System.Environment]::GetEnvironmentVariables('User')
[System.Environment]::GetEnvironmentVariables('Machine')
```

Get a particular environment variable.
```bash
[Environment]::GetEnvironmentVariable("NAME_OF_VARIABLE")
[Environment]::GetEnvironmentVariable("NAME_OF_VARIABLE", "Machine")
[Environment]::GetEnvironmentVariable("NAME_OF_VARIABLE", "User")
```

Set an environment variable
```bash
$Env:NAME_OF_VARIABLE = "value"
```

Remove an environment variable
```bash
Remove-Item Env:\NAME_OF_VARIABLE
```

## History
View where PSReadLine's history is saved to.
```bash
(Get-PSReadlineOption).HistorySavePath
```

Search PSReadLine's history
```bash
Get-Content (Get-PSReadlineOption).HistorySavePath | ? { $_ -like '*searchterm*' }
```

View session error information
```bash
Get-Error -newest 2
Get-Error -last 2
# Names only
$Error
```

Clear last command from history
```bash
Clear-History -Newest -Count 1
```
