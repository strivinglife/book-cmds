# PowerShell Profile

## Refresh PowerShell Profile
```bash
. $profile
```

## My Windows PowerShell Profile
```bash
Import-Module Terminal-Icons
Import-Module PSReadLine

Set-PSReadLineOption -PredictionSource History

oh-my-posh init pwsh --config ~/OneDrive/Apps/oh-my-posh/aritraroy.omp.json | Invoke-Expression
$env:POSH_GIT_ENABLED = $true

function Set-Title {
<#
.Description
Set-Title sets the window/tab title, such as for Windows Terminal.
#>
	param(
		[Parameter(Mandatory = $true)]
		[string]
		$title
	)
	$Host.UI.RawUI.WindowTitle = $title
}

function Set-Title-Folder {
<#
.Description
Set-Title-Folder sets the window/tab title, such as for Windows Terminal, based on the current folder.
#>
	$Host.UI.RawUI.WindowTitle = Split-Path -Path (Get-Location) -Leaf
}

function Start-ExtensionOpen {
<#
.Description
Start-ExtensionOpen opens a single file with a matching extension, if possible.
#>
	param(
		[string]
		$FileExtension
	)
	$matchingFiles = Get-ChildItem . -Filter *.$FileExtension
	$matchCount = ($matchingFiles | Measure-Object).Count

	if ($matchCount -eq 1) {
		Write-Output "Opening $($matchingFiles.Name)"
		Invoke-Item $matchingFiles[0]
	} elseif ($matchCount -eq 0) {
		Write-Error "No matching files for $($FileExtension)"
	} else {
		Write-Output $matchingFiles.Name
	}
}
```
