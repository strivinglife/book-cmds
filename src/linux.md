# Linux
Specifically using Ubuntu.

## SSH
Type `exit` to exit out of a SSH session.

```bash
# SSH into a server with Raspberry Pi's default user.
ssh pi@<ip_address>

# SSH into a server as a particular user.
ssh <user_name>@<ip_address>

# Output current user's name.
whoami

# View last hour of SSH logs.
sudo journalctl -u ssh --since "1 hour ago"

# Determine shell being used.
ps -p "$$"

# Find process having a process id (PID).
ps -p <pid>

# Create a key.
ssh-keygen -b 4096
# View the public key.
cat ~/.ssh/id_rsa.pub
```

## System information
```bash
# View OS information.
cat /etc/os-release

# View system information on Ubuntu, which is displayed on SSH login:
landscape-sysinfo

# System monitoring.
top
# Prettier version of top.
htop

# Memory usage.
free
# Human readable.
free -h
# Show in MBs.
free -m

# IP address
ip a
hostname -I

# Server uptime.
uptime

# View path variable.
echo $PATH

# Find path of binary.
which cd

# Get hostname information, operating system, kernel.
hostnamectl
```

## Package management
```bash
# Show manually installed packages.
apt-mark showmanual
apt list --manual-installed

# List installed packages.
apt list --installed
dpkg --list

sudo dpkg --audit
sudo dpkg --get-selections
```

## Service logs
```bash
# View logs for a particular service.
sudo journalctl -fu mycustom.service
sudo journalctl -fu mycustom.service --since "1 hour ago"
```

## Copy files
```bash
# Copy a file to a directory.
scp file-name.ext <user>@<server>:~/path/to/directory/

# Copy the contents of a folder to a remote folder.
scp -r .\path\to\directory\* <user>@<server>:/path/to/remove/directory
```

## Sudo
```bash
# Start a root shell. `exit` when done.
sudo -s

# Temporarily run as super user to do stuff.
sudo su
# do stuff
exit
```

## Firewall (ufw)
```bash
# Show status of firewall. If active, also lists rules.
sudo ufw status
# Show rules even when ufw is inactive.
sudo ufw show added

# Allow by service.
sudo ufw allow ssh
# Allows 80 and 443.
sudo ufw allow 'Nginx Full'

# Allow port-number.
sudo ufw allow <port-number>
sudo ufw allow 8080

# Remove allowed port-number.
sudo ufw delete allow <port-number>
sudo ufw delete allow 9041
```
