# Windows

Search Files.bat
```bat
REM From https://stackoverflow.com/a/13799990/11912
findstr /s "<term>" *.item
```

## Battery reporting
```bash
# Generate a battery report.
powercfg /batteryreport /output "C:\battery-report.html"

# Sleep study.
powercfg /sleepstudy /output "C:\sleep-study.html"
```

## Setup SSH Key
```bash
ssh-keygen -t ed25519 -C "email@example.com"
# Copy public key in Windows Terminal.
cat ~/.ssh/id_ed25519.pub | clip

# Copy to a Linux server.
type ~\.ssh\id_ed25519.pub | ssh user@host "cat >> .ssh/authorized_keys"
```
