# PostgreSQL
Unless otherwise noted, non-SQL commands are running under Ubuntu.

```bash
# On Ubuntu, run psql as default postgres user.
sudo -u postgres psql

# List all databases.
sudo -u postgres psql -l
```

```sql
-- List all databases.
\l

-- Quit psql.
\q
```

## Configuration

```bash
# Show the full path to the PostgreSQL configuration file.
sudo -u postgres psql -c 'SHOW config_file'
# Example: /etc/postgresql/12/main/postgresql.conf

# Show the full path to the HBA configuration file.
sudo -u postgres psql -c 'SHOW hba_file'
# Example: /etc/postgresql/12/main/pg_hba.conf
```

```sql
-- Locate the HBA configuration file.
SHOW hba_file;
-- Example: /etc/postgresql/12/main/pg_hba.conf

-- Or just query the file for rules.
select * from pg_hba_file_rules();
```

## User management
```sql
-- List all users.
\du

-- Create a new user.
CREATE USER <name>;
```

## Database management
```bash
# Create a new database.
sudo -u postgres createdb <name>
# Verify it was created.
sudo -u postgres psql -l

# Create a new database, echoing out the commands run.
sudo -u postgres createdb <name> -e

# Drop a database.
sudo -u postgres dropdb <name>

```
