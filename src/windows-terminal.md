# Windows Terminal

Open a new Windows Terminal window.
```bash
wt
```

Open a new tab in the current window.

```bash
wt -w 0
```
