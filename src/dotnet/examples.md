# .NET Examples

## Example: .NET API with Angular Frontend and XUnit Testing
This example sets up a new .NET webapi project with XUnit testing and an Angular frontend.

```bash
# From repo root:
dotnet new sln
dotnet new webapi -o API
dotnet sln add API
dotnet new gitignore
git init
git add .
git commit -m "Add new .NET webapi project and solution"

# XUnit project.
dotnet new xunit -o API.Tests
dotnet add .\API.Tests\API.Tests.csproj reference .\API\API.csproj
dotnet sln add API.Tests
git add .
git commit -m "Add new XUnit project"

# Create Angular application.
ng new client
git add .
git commit -m "Add new Angular application"
```

### Project Workspace via Windows Terminal
```bash
wt -d .\ --title 'Repo Root' `; nt -d .\client\ --title 'ng serve' `; split-pane -H -d .\API\ --title 'dotnet watch run' `; nt -d .\client\src\app\ --title 'ng g ...'`; nt -d .\client\ --title 'ng test' `; split-pane -H -d .\ --title 'dotnet test'
```

### Alternatively
```bash
# From repo root:
cd .\API\
dotnet run

# From repo root:
dotnet test

# From repo root:
cd .\client\
ng serve

# From repo root:
cd .\client\
ng test
```
