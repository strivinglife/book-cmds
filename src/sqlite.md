# SQLite

For a CLI on Windows, [download](https://www.sqlite.org/download.html) sqlite-tools-win32-x86-___.zip

## Import a tsv file into a new database.
This assumes the tsv has a header row.

```bash
.open files.sqlite3
.mode tabs
.import files.tsv files
```
