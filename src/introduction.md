# Introduction
The following is a collection of commands for various languages and tools.

This book is [generated with mdBook][mdbook] and hosted by [GitLab Pages](https://gitlab.com/strivinglife/book-cmds).

## See Also
- [Git Commands](https://git.jamesrskemp.com/)

[mdbook]: https://github.com/rust-lang/mdBook
