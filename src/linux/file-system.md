# File system

## Information
```bash
# View disk space.
df
df -h

# Count number of files and folders in home directory.
ls ~ | wc -l

# List block devices. Helps with seeing partition information.
sudo lsblk -f -m

# View the total size of a directory.
sudo du -sh /path/to/directory
# Show by child directory size.
sudo du -h /path/to/directory --max-depth=1
# Show largest directories first.
sudo du -h /path/to/directory --max-depth=1 | sort -hr
# Sort by directory name.
sudo du -h /path/to/directory --max-depth=1 | sort -k2
# Largest directories.
sudo du --separate-dirs -h /path/to/directory | sort -hr | head
sudo du --separate-dirs -h /path/to/directory | sort -hr | head -n 2

# List file sizes (in MB) in a directory.
sudo ls -l --block-size=MB /var/log/nginx/
sudo ls -l --block-size=1MB /var/log/nginx/
# Human readable file sizes.
sudo ls -lh /var/log/nginx/

# Get all .mp4 files sorted by date and showing date, folder name, file name, and size.
# Excludes ._ and .DS_Store files (macOS).
find . -type f -name "*.mp4" ! -name "._*" ! -name ".DS_Store" -printf "%CY-%Cm-%Cd %CT     %h     %f     %s\n" | sort -n
```

## Navigation
```bash
# Print working directory (current directory).
pwd

# By itself, move to the current user's home directory. Tilde is short for home directory.
cd
cd ~
```

## Modification
```bash
# Make a new directory.
mkdir name
mkdir name1 name2 name3

# Make a new directory and sub-directory/ies.
mkdir -p path/to/directory

# Move file(s)/directories to an existing directory.
mv file.txt dir1
mv file1.txt file2.txt dir1
mv file1.txt file2.txt new-dir dir1

# Rename a file.
mv file.txt new-name.txt

# Rename a directory.
mv dir-name new-dir-name

# Copy a file
cp path/to/file.txt .
cp file.txt copy-file.txt

# Remove/delete a file.
rm file.txt

# Remove/delete an empty directory.
rmdir path/to/directory

# Remove/delete a directory, even if it has files. (Only macOS?)
rmdir -r path/to/directory
# Works on Ubuntu.
rm -r path/to/directory
```

## File viewing and creation
```bash
# Look at a file or multiple files.
cat file.txt
cat file1.txt file2.txt
cat file?.txt
cat file*

# Write to a file.
echo "Some text" > file.txt
echo "Some more text" >> file.txt

# File viewer. Press q to quit.
less file.txt

# List all files in a directory, with full paths, into a text file.
find wwwroot -type f > files.txt
# The above with directory, file name, file date, and size, into a tab-separated file.
find . -type f ! -name "._*" ! -name ".DS_Store" -printf "%h\t%f\t%CY-%Cm-%Cd %CT\t%s\n" > files.tsv

# Get full path to a file.
readlink -f relative/path/to/file

# Read lines of a file
sudo head -n 10 /var/log/nginx/access.log
sudo tail -n 10 /var/log/nginx/access.log
```

## Hidden content
```bash
# Make a hidden directory.
mkdir .dir1

# Make a hidden file.
mkdir .hide.txt

# List all hidden files/folders.
ls -a
```
