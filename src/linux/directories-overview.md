# Linux directories overview

From root (`cd /`):

- `/bin` essential executables, always available
- `/sbin` essential super user executables
- `/lib` shared common libraries (for `bin` and `sbin`)
- `/etc` (editable text configuration)
- `/usr`
	- `/usr/local`
		- `/usr/local/bin` locally compiled binaries
	- `/usr/bin` installed binaries for users
- `/home` user data
	- `/home/___` individual user directory (also `~`)
- `/boot` required to boot the system (like Linux kernel)
- `/dev` devices/hardware/drivers
- `/opt` optional/add-on software, rarely will be used
- `/var` variable files that change as the system is used (like log and cache files)
- `/tmp` temporary files
- `/proc` running processes

Detailed in the [Filesystem Hierarchy Standard](https://refspecs.linuxfoundation.org/FHS_3.0/fhs/index.html).
