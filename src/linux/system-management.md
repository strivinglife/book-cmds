# Linux system management
```bash
# Reboot/restart.
sudo reboot
sudo shutdown -r now
# Reboot in 5 minutes.
sudo shutdown -r 5

# Shutdown
sudo poweroff

# Get machine and kernel information.
uname -mrs

# Get distribution-specific information.
lsb_release -a

# Get computer name.
hostname

# Get Debian version.
cat /etc/debian_version
```

## Updates
```bash
# Get a list of current packages.
sudo apt update

# Get a list of upgradable packages.
apt list --upgradable
apt list --upgradable -a

# Upgrade all packages.
sudo apt upgrade

# Install or update specificed package.
sudo apt install <package-name>
```
