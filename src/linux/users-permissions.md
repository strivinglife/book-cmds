# Users and permissions

## User management
```bash
# Show who's logged in.
w

# Show the last logged in users.
last

# Show the last bad login attempts.
lastb

# Set the password for a user.
passwd <user>

# List user information, including groups.
id <user>

# List all users.
cat /etc/passwd

# List all users across multiple sources.
getent passwd

# Install sudo on Debian or the like (that don't have it by default).
apt update
apt install sudo

# Grant a user sudo.
usermod -aG sudo <username>
# Add the current user to a group (<group-name>).
sudo usermod -aG <group-name> ${USER}
```

## Permissions
```bash
# List all users.
compgen -u
getent passwd

# List all groups.
compgen -g
getent group

# List all groups current user is in.
groups
# List all groups a user (username) is in.
groups username

# Add write access to group.
chmod g+w file-or-directory

# Remove write and execute from group.
chmod g-wx file-or-directory

# Remove read, write, and execute from others.
chmod o-rwx file-or-directory

# Users, groups, and others have read, write, and execute.
chmod ugo+rwx file-or-directory

# Grant all users read-only access to a file or directory.
chmod a=r file-or-directory

# View permissions.
ls -l
ls -ld
```
