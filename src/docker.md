# Docker
> Unless otherwise noted, commands run on Ubuntu 22.

## Setup
```bash
# Add the current user to the docker group so docker can be run without sudo.
sudo usermod -aG docker ${USER}
su - ${USER}
exit
```

## Basics
```bash
# Get installed version.
docker -v

# Verify Docker is setup / run the Hello World image.
sudo docker run hello-world

# Check whether Docker is running.
sudo systemctl status docker
sudo systemctl is-active docker

# Search Docker Hub for all images matching a search term (<term>).
docker search <term>
```

## Images
```bash
# Show all downloaded images.
docker images

# Remove an image.
docker rmi <image-id>

# View information about an image.
docker inspect <image-name>
# Get the size of an image from the above.
docker inspect -f "{{ .Size }}" <image-name>

# View the history/layers of an image (how it was built).
docker history <image-id>
docker history <image-name>
```


### Image Tags
```bash
# Give an image/tag an additional tag.
docker tag <image-name>:<tag-name> <image-name>:<another-tag-name>

# These extra tags can then be removed, while keeping the image.
docker tag <image-name>:<another-tag-name>
```

## Containers
```bash
# List running containers.
docker ps
# List all containers.
docker ps -a

# Start a container with <container-id> or <container-name>.
docker start <container-id>
docker start <container-name>

# Stop a container.
docker stop <container>

# Restart a container.
docker restart <container>

# Remove/delete a container.
docker rm <container>

# View container's logs.
docker logs <container>
```

### Working with Running Containers
```bash
docker cp path/to/file <container>:./destination/path/

# Run bash on the container, if installed.
docker exec -it <container> bash
```

## Volumes
```bash
# List volumes.
docker volume ls

# View information about volume.
docker volume inspect <volume-name>

# Delete a volume.
docker volume rm <volume-name>
```

### Volume content listing
From [How to list the content of a named volume in docker 1.9+?](https://stackoverflow.com/a/61655412/11912).

```bash
# View a specific volume.
ls -las $(docker volume inspect <volume-name> | grep Mountpoint | cut -d\" -f 4)

# View all volumes.
for i in  `docker volume ls -q`; do echo volume: ${i}; \
ls -las $(docker volume inspect $i | grep Mountpoint | cut -d\" -f 4); \
done
```

## System
```bash
# Show disk usage.
docker system df
docker system df --verbose

# Show active container stats.
docker stats

# View and prune images.
docker image ls --filter dangling=true
docker image prune
```

## Compose
```bash
# Stop based upon docker-compose.yml in directory.
docker compose stop

# Start services based upon docker-compose.yml.
docker compose up -d

# View the config of a docker-compose.yml in current directory.
docker compose config
```

## Install on Ubuntu
On Ubuntu, from https://docs.docker.com/engine/install/ubuntu/:

```bash
sudo apt-get update
sudo apt-get install ca-certificates curl gnupg

sudo install -m 0755 -d /etc/apt/keyrings
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /etc/apt/keyrings/docker.gpg
sudo chmod a+r /etc/apt/keyrings/docker.gpg

echo \
  "deb [arch="$(dpkg --print-architecture)" signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/ubuntu \
  "$(. /etc/os-release && echo "$VERSION_CODENAME")" stable" | \
  sudo tee /etc/apt/sources.list.d/docker.list > /dev/null

sudo apt-get update

sudo apt-get install docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin

# Test.
sudo docker run hello-world
```
