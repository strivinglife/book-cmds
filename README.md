# book-cmds
This project supports [cmds.jamesrskemp.com](https://cmds.jamesrskemp.com/), which is a collection of varous commands, for various programs.

Git-specific commands are documented at [git.jamesrskemp.com](https://git.jamesrskemp.com).

This site is possible thanks to [mdbook](https://github.com/rust-lang/mdBook) and [GitLab pages](https://about.gitlab.com/product/pages/).
